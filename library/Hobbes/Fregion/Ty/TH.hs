module Hobbes.Fregion.Ty.TH ( makeDesc
                            ) where

import Language.Haskell.TH as TH
import Language.Haskell.TH.Syntax (Q(..))
import Hobbes.Fregion.Ty.Desc as Ty

makeDesc :: Name -> Q [Dec]
makeDesc = (withInfo =<<)  . reify

withInfo :: Info -> Q [Dec]
withInfo (TyConI (DataD _ n _ _ cons _)) = makeDescImpl n cons
withInfo (TyConI (NewtypeD _ n _ _ con _)) = makeDescImpl n [con]

typeToDesc :: TH.Type -> Q Exp
typeToDesc (ConT tpN) = [e| desc @ $(conT tpN)|]
typeToDesc (AppT ListT tp) = [e| $(typeToDesc tp) >>= mkDArrTy |]

makeDescImpl :: Name -> [Con] -> Q [Dec]
makeDescImpl n cons =
  case cons of
    [c] -> thMkProduct c
    _:_ -> thMkSum cons
    _ -> [d|
           instance (Ty.Interpreter rep) => Ty.HasDesc $(conT n) rep where
             desc = mkVoidTy
           |]
    where
      thBuildProd :: Integer -> String -> TH.Type -> Q Exp
      thBuildProd i s tp = do
          [e|
            do
              tpd <- $(typeToDesc tp)
              mkProdTy (return [(s, i, tpd)])
            |]

      fieldsProductQ :: Q Exp -> (Integer, (String, TH.Type)) -> Q Exp
      fieldsProductQ a (i, (s, tp)) = [e|
                                        do
                                          let
                                            mergeTys :: forall rep . (Ty.Interpreter rep) => String -> Ty.Desc -> Ty.Desc -> rep (String, Ty.Desc)
                                            mergeTys conNm MkVoidTy ntp = return (conNm, ntp)
                                            mergeTys conNm (MkProdTy accTy) (MkProdTy ty) = mkProdTy (return $ accTy++ty) >>= return . ((,) conNm)
                                          (conNm, accTy) <- $(a)
                                          fldTy <- $(thBuildProd i s tp)
                                          mergeTys conNm accTy fldTy
                                        |]

      thOnProduct :: String -> [(String, TH.Type)] -> Q [Dec]
      thOnProduct conNm [] = [d|
                               instance (Ty.Interpreter rep) => Ty.HasDesc $(conT n) rep where
                                 desc = mkSumTy . return . return $ (conNm, MkPrimTy "unit" MkVoidTy)
                               |]
      thOnProduct conNm sts = [d|
                                instance (Ty.Interpreter rep) => Ty.HasDesc $(conT n) rep where
                                  desc = do
                                         p <- $(foldl fieldsProductQ [e|return (conNm, MkVoidTy)|] (zip [0..] sts))
                                         mkSumTy . return . return $ p
                                |]

      thMkProduct :: Con -> Q [Dec]
      thMkProduct (RecC conNm fields) = thOnProduct (nameBase conNm) $ fmap (\(nf,_,t) -> (nameBase nf, t)) fields
      thMkProduct (NormalC conNm fields) = thOnProduct (nameBase conNm) $ zipWith (\fn (_,t) -> (fn,t)) ([".f"++show(i) | i<-[0..]::[Int]]) fields

      thFieldProduct :: String -> [(String, TH.Type)] -> Q Exp
      thFieldProduct conNm flds = [e|
                                     $(foldl fieldsProductQ [e|return (conNm, MkVoidTy)|] (zip [0..] flds))
                                    |]

      thMkSum :: [Con] -> Q [Dec]
      thMkSum cs@(_:_) = do
        let
          conToExpQ :: Q Exp -> (Int, Con) -> Q Exp
          conToExpQ a (_, (RecC nr [(_, _, fTy)])) =
            let
              nb = nameBase nr
            in
              [e| do
                    d <- $(typeToDesc fTy)
                    acc <- $(a)
                    return ((nb,d):acc)
                |]

          conToExpQ a (_, (RecC nr [])) =
            let
              nb = nameBase nr
            in
              [e| do
                    d <- mkUnitTy
                    acc <- $(a)
                    return ((nb,d):acc)
                |]

          conToExpQ a (_, (RecC nr fields)) = [e| do
                                                   (nm, d) <- $(thFieldProduct (nameBase nr) $ fmap (\(nf,_,t) -> (nameBase nf, t)) $ fields)
                                                   acc <- $(a)
                                                   return ((nm, d):acc)
                                                |]

          conToExpQ a (_, (NormalC nr [])) =
            let
              nb = nameBase nr
            in
              [e| do
                    d <- mkUnitTy
                    acc <- $(a)
                    return ((nb,d):acc)
                |]

          conToExpQ a (_, (NormalC nr [(_, tp)])) =
            let
              nb = nameBase nr
            in
              [e| do
                    d <- $(typeToDesc tp)
                    acc <- $(a)
                    return ((nb, d):acc)
                |]

          conToExpQ a (_, (NormalC nr fields)) = [e| do
                                                      (nm, d) <- $(thFieldProduct (nameBase nr) $ zip ([".f"++show(j) | j<-[0..]::[Int]]) . fmap (\(_,t) -> t) $ fields)
                                                      acc <- $(a)
                                                      return ((nm, d):acc)
                                                    |]

          conToExpQ a _ = a

        [d|
          instance (Ty.Interpreter rep) => Ty.HasDesc $(conT n) rep where
            desc = mkSumTy (reverse <$> $(foldl conToExpQ [e|return []|] (zip [0..] cs)))
          |]

