-- It is generally a good idea to keep all your business logic in your library
-- and only use it in the executable. Doing so allows others to use what you
-- wrote in their libraries.

import Hobbes.Fregion.Writer.Writer
import System.IO
import System.Directory
import Control.Concurrent
import Control.Exception
import qualified Data.ByteString.Char8 as C
import Data.IORef

import GHC
import GHC.Paths (libdir)
import HscTypes
import Name
import TyCon
import TyCoRep
import Type
import DataCon

import Text.Show.Pretty

loop :: IO Bool -> IO () -> IO ()
loop iob io = iob >>= \cond -> if cond
                               then io >> loop iob io
                               else return ()

withTempWriter :: String -> (Writer -> IO ()) -> IO ()
withTempWriter str ioFn = do
  bracket
    (getTemporaryDirectory)
    (\dp -> bracket
            (openTempFile dp str)
            (\(fp, h) -> do
                hClose h
                unlinkFilePath (C.pack fp)
                w <- newWriter (C.pack fp)
                ioFn w)
            (\(_, h) -> hClose h)
    )
    (\_ -> return ())

data DataType where
  MkDataType :: { dtName        :: String
                , dtModName     :: String
                , dtIsNewType   :: Bool
                , dtDesc        :: Desc
                , dtIsRecursive :: Bool
                } -> DataType
  deriving (Show)

data Metadata where
  MkD :: DataType -> Metadata
  MkS :: { mdSName :: String
         , mdSDesc :: Desc
         } -> Metadata
  MkC :: { mdCName :: String
         } -> Metadata
  deriving (Show)

data Desc where
  MkProduct :: [Desc] -> Desc
  MkSum     :: [Desc] -> Desc
  MkM1      :: Metadata -> Desc
  MkK1      :: String -> Desc
  MkV1      :: Desc
  deriving (Show)

isProduct :: TyCon -> Bool
isProduct = isProductTyCon

fieldNames :: DataCon -> [FieldLabel]
fieldNames = dataConFieldLabels

isRecord :: DataCon -> Bool
isRecord = (> 0) . length . fieldNames

isDProduct :: DataCon -> Bool
isDProduct = (> 0) . dataConRepArity

isVoid :: TyCon -> Bool
isVoid = (== 0) . length . tyConDataCons

modString :: TyCon -> String
modString = moduleNameString . moduleName . nameModule . tyConName

showTy :: Type -> String
showTy (TyVarTy tv) = getOccString tv
showTy (TyConApp tc tys) = getOccString tc
showTy (FunTy a b) = showTy a ++ "->" ++ showTy b
showTy (AppTy a b) = showTy a ++ " " ++ showTy b
showTy (ForAllTy _ b) = showTy b

mkRProduct :: DataCon -> [Desc]
mkRProduct dc = [MkM1 (MkS (getOccString $ flSelector fld) ty) | (fld, ty) <- zip (fieldNames dc) (mkDProduct dc)]

mkDProduct :: DataCon -> [Desc]
mkDProduct dc = [ MkK1 (showTy arg) | arg <- dataConOrigArgTys dc]

mkProduct :: TyCon -> [Desc]
mkProduct = fmap go . tyConDataCons
  where
    go :: DataCon -> Desc
    go dc@(isRecord -> True) = MkProduct . mkRProduct $ dc
    go dc@(isDProduct -> True) = MkProduct . mkDProduct $ dc
    go dc = MkM1 . MkC . conName $ dc

    conName :: DataCon -> String
    conName = getOccString . dataConName

isRecursiveTyCon :: TyCon -> Bool
isRecursiveTyCon _ = False

deconstruct :: [TyCon] -> [Desc]
deconstruct = fmap go
  where
    go :: TyCon -> Desc
    go tc@(isProduct -> True) = MkM1 $ MkD $ MkDataType { dtName = (getOccString . tyConName) tc
                                                        , dtModName = modString tc
                                                        , dtIsNewType = isNewTyCon tc
                                                        , dtDesc = MkProduct (mkProduct tc)
                                                        , dtIsRecursive = isRecursiveTyCon tc
                                                        }
    go tc@(isVoid -> True) = MkM1 $ MkD $ MkDataType { dtName = (getOccString . tyConName) tc
                                                     , dtModName = modString tc
                                                     , dtIsNewType = isNewTyCon tc
                                                     , dtDesc = MkV1
                                                     , dtIsRecursive = isRecursiveTyCon tc
                                                     }
    go tc = MkM1 $ MkD $ MkDataType { dtName = (getOccString . tyConName) tc
                                    , dtModName = modString tc
                                    , dtIsNewType = isNewTyCon tc
                                    , dtDesc = MkSum (mkProduct tc)
                                    , dtIsRecursive = isRecursiveTyCon tc
                                    }

rGhc :: IO [Desc]
rGhc = do
  runGhc (Just libdir) $ do
    getSessionDynFlags >>= setSessionDynFlags
    let
      mn = mkModuleName "Test"

    addTarget Target { targetId = TargetModule mn
                     , targetAllowObjCode = True
                     , targetContents = Nothing
                     }
    load LoadAllTargets
    ms <- getModSummary mn
    pm <- parseModule ms
    tc <- typecheckModule pm
    dm@(DesugaredModule tcmod modguts) <- desugarModule tc
    let
      tyCons = mg_tcs modguts

    return $ deconstruct tyCons

main :: IO ()
main = do
  ioref <- newIORef 0
  _ <- loop
       (atomicModifyIORef' ioref (\i -> (i+1, if i < 10 then True else False)))
       (withTempWriter "file..db" (\w -> C.hPutStrLn stdout (wrFilePath w)))
  rGhc >>= C.putStrLn . C.pack . ppShow
  return ()
