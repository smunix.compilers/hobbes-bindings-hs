module Hobbes.Fregion.Writer.Writer ( Writer
                                    , newWriter
                                    -- , withWriter
                                    , wrFilePath
                                    , unlinkFilePath
                                    ) where
import Data.IORef
import Foreign.C
import Foreign.C.String
import Foreign.Ptr
import Foreign.ForeignPtr
import qualified Data.ByteString.Char8 as C
import qualified Data.ByteString.Internal as BI

data Writer = Writer { wrImage :: IORef (ForeignPtr Writer)
                     , wrFilePath :: C.ByteString
                     }

foreign import ccall "mkWriter" c_mkWriter :: Ptr CChar -> CLong -> IO (Ptr Writer)
foreign import ccall "&delWriter" c_delWriter :: FunPtr(Ptr Writer -> IO())

newWriter :: C.ByteString -> IO Writer
newWriter bstr@(BI.PS fp off len) = withForeignPtr fp $ \base -> do
  w <- c_mkWriter (castPtr (plusPtr base off)) (fromIntegral len)
  imgRef <- newIORef =<< newForeignPtr c_delWriter w
  return $ Writer imgRef bstr

withWriter :: Writer -> (Ptr Writer -> IO a) -> IO a
withWriter (Writer ioref bstr) fn = do
  fp <- readIORef ioref
  withForeignPtr fp $ \w -> fn w


foreign import ccall "unlink" c_unlink :: Ptr CChar -> IO ()
unlinkFilePath :: C.ByteString -> IO ()
unlinkFilePath = flip withCString c_unlink . C.unpack

