module Hobbes.Fregion.Ty.Hobbes ( Hobbes(..)
                                , mkState
                                ) where
import Hobbes.Fregion.Ty.Desc
import Hobbes.Fregion.Ty.CalcSizeOf
import Data.Proxy

data State where
  State :: { memOffset :: Int} -> State

mkState :: Int -> State
mkState mo = State { memOffset = mo }

initState :: State
initState = mkState 0

newtype Hobbes a = Hobbes { runHobbes :: forall m . (Monad m) => State -> m (State, a) }
  deriving (Functor)

instance Applicative (Hobbes) where
  pure a = Hobbes $ \s -> return (s, a)
  (Hobbes f) <*> (Hobbes a) = Hobbes $ \s -> do
    (_,a') <- a s
    (_,f') <- f s
    return (s, f' a')

instance Monad (Hobbes) where
  (Hobbes a) >>= fM = Hobbes $ \s -> do
    (as, a') <- a s
    (runHobbes $ fM a') as

instance CalcSizeOf (Hobbes) where
  reset = Hobbes $ \_ -> return (initState, ())
  offset = Hobbes $ \s -> return (s, memOffset s)
  newField (Proxy :: Proxy f) = Hobbes $ \s -> do
    (_, c) <- runHobbes offset s
    (_, a) <- runHobbes (memAlign @f) s
    (_, sz) <- runHobbes (memSize @f) s

    let
      d = max a sz

    if (a==0)
      then return (s{memOffset=c}, memOffset s)
      else (case divMod c a of
               (_,0) -> return (s{memOffset=c+d}, memOffset s)
               (q, _) -> return (s{memOffset=(1+q)*a+d}, (1+q)*a))
