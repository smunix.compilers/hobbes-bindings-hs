module Hobbes.Fregion.Ty.CalcSizeOf ( CalcSizeOf(..)
                                    ) where

import Hobbes.Fregion.Ty.Desc as Desc (HasMemAlign, HasMemSize)
import Data.Proxy
import Data.Kind (Type)

class (Monad m) => CalcSizeOf m where
  reset :: m ()
  offset :: m Int
  newField :: forall f . (HasMemAlign f, HasMemSize f) => Proxy (f :: Type) -> m Int


