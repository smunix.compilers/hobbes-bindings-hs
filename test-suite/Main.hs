{-# OPTIONS_GHC -F -pgmF tasty-discover #-}

main :: IO ()
main = do
    test <- testSpec "hobbes-hs" spec
    Test.Tasty.defaultMain test

