module Hobbes.Fregion.Ty.Desc.TestHobbes ( spec_Ty_Desc_Hobbes
                                         ) where


import Test.Tasty.Hspec

import Hobbes.Fregion.Ty.TH (makeDesc)
import Hobbes.Fregion.Ty.Hobbes as Hobbes
import Hobbes.Fregion.Ty.CalcSizeOf as CalcSizeOf

import Data.Word
import Data.Int
import Data.Proxy

data VariantT = Chicken String
              | Dog Int
              | Fish Double
makeDesc ''VariantT

data VoidT
makeDesc ''VoidT

data UnitT = UnitT
makeDesc ''UnitT

data OneT = OneT String
makeDesc ''OneT

data DoubleT = DoubleT Int String
makeDesc ''DoubleT

data SumProdT = ProdC { name :: String
                      , ages :: Int
                      , friends :: [String]
                      }
              | Empty {}
              | Nothing
              | NestedVariant VariantT
              | OneInt Int
              | OneChar Char
              | OneString String
              | OneCharArr [Char]
              | DoubleIntChar Int Char

makeDesc ''SumProdT

newtype ProdSumProdT = ProdSumProdT { unProdSumProdT :: [[SumProdT]] }
makeDesc ''ProdSumProdT

spec_Ty_Desc_Hobbes :: Spec
spec_Ty_Desc_Hobbes = parallel $ do
  describe "AlignInterpreter" $ do
    describe "size/alignments" $ do
      do
        (_, a) <- runHobbes (do
                                CalcSizeOf.reset
                                CalcSizeOf.newField (Proxy @Bool)
                                CalcSizeOf.offset) (mkState 0)
        it "Bool" $ a `shouldBe` 1
      do
        (_, a) <- runHobbes (do
                               CalcSizeOf.reset
                               CalcSizeOf.newField (Proxy @Char)
                               CalcSizeOf.offset) (mkState 0)
        it "Char" $ a `shouldBe` 1
      do
        (_, (b,w,s)) <- runHobbes (do
                                      CalcSizeOf.reset
                                      b <- CalcSizeOf.newField (Proxy @Bool)
                                      w <- CalcSizeOf.newField (Proxy @Word64)
                                      s <- CalcSizeOf.offset
                                      return (b, w, s)) (mkState 0)
        it "BoolxWord64" $ (b,w,s) `shouldBe` (0,8,16)
      do
        (_, a) <- runHobbes (do
                               CalcSizeOf.reset
                               CalcSizeOf.newField (Proxy @Bool)
                               CalcSizeOf.newField (Proxy @Word32)
                               CalcSizeOf.offset) (mkState 0)
        it "CharxWord32" $ a `shouldBe` 8
      do
        (_, a) <- runHobbes (do
                               CalcSizeOf.reset
                               CalcSizeOf.newField (Proxy @Int16)
                               CalcSizeOf.newField (Proxy @Int16)
                               CalcSizeOf.offset) (mkState 0)
        it "Int16xInt16" $ a `shouldBe` 4
      do
        (_, a) <- runHobbes (do
                               CalcSizeOf.reset
                               CalcSizeOf.newField (Proxy @Int16)
                               CalcSizeOf.newField (Proxy @Int32)
                               CalcSizeOf.offset) (mkState 0)
        it "Int16xInt32" $ a `shouldBe` 8
      do
        (_, a) <- runHobbes (do
                               CalcSizeOf.reset
                               CalcSizeOf.newField (Proxy @Int32)
                               CalcSizeOf.newField (Proxy @Int16)
                               CalcSizeOf.offset) (mkState 0)
        it "Int32xInt16" $ a `shouldBe` 6
      do
        (_, a) <- runHobbes (do
                               CalcSizeOf.reset
                               CalcSizeOf.newField (Proxy @String)
                               CalcSizeOf.offset) (mkState 0)
        it "String" $ a `shouldBe` 8
      do
        (_, a) <- runHobbes (do
                               CalcSizeOf.reset
                               CalcSizeOf.newField (Proxy @[Char])
                               CalcSizeOf.offset) (mkState 0)
        it "[Char]" $ a `shouldBe` 8
      do
        (_, a) <- runHobbes (do
                               CalcSizeOf.reset
                               CalcSizeOf.newField (Proxy @[Int])
                               CalcSizeOf.offset) (mkState 0)
        it "[Int]" $ a `shouldBe` 8
      do
        (_, a) <- runHobbes (do
                               CalcSizeOf.reset
                               CalcSizeOf.newField (Proxy @[[[Int]]])
                               CalcSizeOf.offset) (mkState 0)
        it "[[[Int]]]" $ a `shouldBe` 8
      do
        (_, a) <- runHobbes (do
                               CalcSizeOf.reset
                               CalcSizeOf.newField (Proxy @())
                               CalcSizeOf.offset) (mkState 0)
        it "()" $ a `shouldBe` 0
      do
        (_, a) <- runHobbes (do
                               CalcSizeOf.reset
                               CalcSizeOf.newField (Proxy @[()])
                               CalcSizeOf.offset) (mkState 0)
        it "[()]" $ a `shouldBe` 0
      do
        (_, a) <- runHobbes (do
                               CalcSizeOf.reset
                               CalcSizeOf.newField (Proxy @[[[()]]])
                               CalcSizeOf.offset) (mkState 0)
        it "[[[()]]]" $ a `shouldBe` 0

