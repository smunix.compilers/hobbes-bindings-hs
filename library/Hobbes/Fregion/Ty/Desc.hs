module Hobbes.Fregion.Ty.Desc ( Desc(..)
                              , HasDesc(..)
                              , HasPrimDesc(..)
                              , HasMemSize(..)
                              , HasPrimMemSize(..)
                              , HasMemAlign(..)
                              , HasPrimMemAlign(..)
                              , Interpreter(..)
                              ) where

import Data.Proxy
import Data.Kind (Type)

import GHC.TypeLits ( Symbol
                    , KnownSymbol
                    , symbolVal
                    , Nat
                    , KnownNat
                    , natVal
                    )

import Data.Word
import Data.Int

data Desc where
  MkVoidTy :: Desc
  MkPrimTy :: String -> Desc -> Desc
  MkVarTy  :: String -> Desc
  MkProdTy :: [(String, Int, Desc)] -> Desc
  MkSumTy  :: [(String, Desc)] -> Desc
  MkFArrTy :: Int -> Desc -> Desc
  MkDArrTy :: Desc -> Desc
  deriving (Eq, Show)

class (Monad rep) => Interpreter rep where
  mkVoidTy :: rep Desc
  mkVoidTy = return MkVoidTy

  mkUnitTy :: rep Desc
  mkUnitTy = desc @()

  mkPrimTy :: String -> Desc -> rep Desc
  mkPrimTy s = return . MkPrimTy s

  mkProdTy :: rep [(String, Int, Desc)] -> rep Desc
  mkProdTy flds = (return . MkProdTy) =<< flds

  mkSumTy :: rep [(String, Desc)] -> rep Desc
  mkSumTy s = (return . MkSumTy) =<< s

  mkDArrTy :: Desc -> rep Desc
  mkDArrTy = return . MkDArrTy

instance (Monad m) => Interpreter m

{- primitives -}
class HasPrimDesc (t :: Type) (s::Symbol) | t -> s where
  primDesc :: (Interpreter m) => m Desc
  default primDesc :: (Interpreter m, KnownSymbol s) => m Desc
  primDesc = mkPrimTy (symbolVal @s Proxy) MkVoidTy

instance HasPrimDesc ()     "unit"
instance HasPrimDesc Bool   "bool"
instance HasPrimDesc Char   "char"
instance HasPrimDesc Word8  "byte"
instance HasPrimDesc Word16 "short"
instance HasPrimDesc Word32 "int"
instance HasPrimDesc Word64 "long"
instance HasPrimDesc Int    "int"
instance HasPrimDesc Int8   "char"
instance HasPrimDesc Int16  "short"
instance HasPrimDesc Int32  "int"
instance HasPrimDesc Int64  "long"
instance HasPrimDesc Float  "float"
instance HasPrimDesc Double "double"

class HasPrimMemSize (t :: Type) (n :: Nat) | t -> n where
  primMemSize :: (Interpreter m) => m Int
  default primMemSize :: (KnownNat n, Interpreter m) => m Int
  primMemSize = return . fromInteger . natVal @n $ Proxy

instance HasPrimMemSize ()     0
instance HasPrimMemSize Bool   1
instance HasPrimMemSize Char   1
instance HasPrimMemSize Word8  1
instance HasPrimMemSize Word16 2
instance HasPrimMemSize Word32 4
instance HasPrimMemSize Word64 8
instance HasPrimMemSize Int    4
instance HasPrimMemSize Int8   1
instance HasPrimMemSize Int16  2
instance HasPrimMemSize Int32  4
instance HasPrimMemSize Int64  8
instance HasPrimMemSize Float  4
instance HasPrimMemSize Double 8

data KindOf where
  PrimTy :: KindOf
  ListTy :: KindOf
  UnitTy :: KindOf
  UnitListTy :: KindOf

type family GetRepOf (a :: Type) :: Type where
  GetRepOf () = ()
  GetRepOf [()] = [()]
  GetRepOf [[a]] = GetRepOf [a]
  GetRepOf a = a

type family GetKindOf (a :: Type) :: KindOf where
  GetKindOf () = UnitTy
  GetKindOf [()] = UnitListTy
  GetKindOf [a] = ListTy
  GetKindOf a = PrimTy

class (GetKindOf a ~ b) => HasMemSizeHelper (a :: Type) (b :: KindOf) where
  memSizeHelp :: forall m . (Interpreter m) => m Int

instance (GetKindOf () ~ UnitTy) => HasMemSizeHelper () UnitTy where
  memSizeHelp = primMemSize @()

instance (GetKindOf [()] ~ UnitListTy) => HasMemSizeHelper [()] UnitListTy where
  memSizeHelp = primMemSize @()

instance (GetKindOf a ~ PrimTy, HasPrimMemSize a n) => HasMemSizeHelper a PrimTy where
  memSizeHelp = primMemSize @a

instance (GetKindOf a ~ ListTy) => HasMemSizeHelper a ListTy where
  memSizeHelp = primMemSize @Word64

class HasMemSize (t :: Type) where
  memSize :: (Interpreter m) => m Int

instance (HasMemSizeHelper (GetRepOf t) (GetKindOf (GetRepOf t))) => HasMemSize t where
  memSize = memSizeHelp @(GetRepOf t) @(GetKindOf (GetRepOf t))

class (HasPrimMemSize t n) => HasPrimMemAlign (t :: Type) (n :: Nat) | t -> n where
  primMemAlign :: (Interpreter m) => m Int
  primMemAlign = primMemSize @t @n

instance (HasPrimMemSize t n) => HasPrimMemAlign t n

class HasMemAlign (t :: Type) where
  memAlign :: (Interpreter m) => m Int

class (GetKindOf t ~ b) => HasMemAlignHelper (t :: Type) (b :: KindOf) where
  memAlignHelp :: (Interpreter m) => m Int

instance (GetKindOf () ~ UnitTy) => HasMemAlignHelper () UnitTy where
  memAlignHelp = primMemAlign @()

instance (GetKindOf t ~ PrimTy, HasPrimMemSize t n) => HasMemAlignHelper t PrimTy where
  memAlignHelp = primMemAlign @t

instance (GetKindOf t ~ ListTy) => HasMemAlignHelper t ListTy where
  memAlignHelp = primMemAlign @Word64

instance (GetKindOf t ~ UnitListTy) => HasMemAlignHelper t UnitListTy where
  memAlignHelp = primMemAlign @()

instance (HasMemAlignHelper (GetRepOf t) (GetKindOf (GetRepOf t))) => HasMemAlign (t :: Type) where
  memAlign = memAlignHelp @(GetRepOf t) @(GetKindOf (GetRepOf t))

class (Interpreter m) => HasDesc (t :: Type) m where
  desc :: m Desc
  namedDesc :: String -> m Desc
  namedDesc s = (desc @t @m) >>= mkPrimTy s

instance {-# OVERLAPPABLE #-} (HasPrimDesc t s, Interpreter m) => HasDesc t m where
  desc = primDesc @t

instance (Interpreter m) => HasDesc String m where
  desc = (desc @Char) >>= mkDArrTy
