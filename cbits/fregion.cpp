#include <fregion.H>

extern "C" {
  hobbes::fregion::writer* mkWriter(char const* fp, size_t len) {
    using namespace hobbes;
    std::cerr << "creating ... [" << std::string(fp, len) << "]" << std::endl;
    auto w = new fregion::writer(std::string(fp, len));
    std::cerr << "created " << std::string(fp, len) << " at [0x" << std::hex << w << "]" << std::endl;
    return w;
  }
  void delWriter(hobbes::fregion::writer* w) {
    std::cerr << "deleting ... [0x" << std::hex << w << "]" << std::endl;
    delete w;
    std::cerr << "deleted [0x" << std::hex << w << "]" << std::endl;
  }
}
