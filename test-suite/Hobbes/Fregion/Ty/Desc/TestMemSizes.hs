module Hobbes.Fregion.Ty.Desc.TestMemSizes ( spec_Ty_Desc_Mem_Sizes
                                           ) where


import Test.Tasty.Hspec

import Hobbes.Fregion.Ty.Desc
import Hobbes.Fregion.Ty.TH (makeDesc)

import Data.Word
import Data.Int

data VariantT = Chicken String
              | Dog Int
              | Fish Double
makeDesc ''VariantT

data VoidT
makeDesc ''VoidT

data UnitT = UnitT
makeDesc ''UnitT

data OneT = OneT String
makeDesc ''OneT

data DoubleT = DoubleT Int String
makeDesc ''DoubleT

data SumProdT = ProdC { name :: String
                      , ages :: Int
                      , friends :: [String]
                      }
              | Empty {}
              | Nothing
              | NestedVariant VariantT
              | OneInt Int
              | OneChar Char
              | OneString String
              | OneCharArr [Char]
              | DoubleIntChar Int Char

makeDesc ''SumProdT

newtype ProdSumProdT = ProdSumProdT { unProdSumProdT :: [[SumProdT]] }
makeDesc ''ProdSumProdT

spec_Ty_Desc_Mem_Sizes :: Spec
spec_Ty_Desc_Mem_Sizes = parallel $ do
  describe "HasMemSize" $ do
    it "()"     $ memSize @()     >>= shouldBe 0
    it "Bool"   $ memSize @Bool   >>= shouldBe 1
    it "Char"   $ memSize @Char   >>= shouldBe 1
    it "Word8"  $ memSize @Word8  >>= shouldBe 1
    it "Word16" $ memSize @Word16 >>= shouldBe 2
    it "Word32" $ memSize @Word32 >>= shouldBe 4
    it "Word64" $ memSize @Word64 >>= shouldBe 8
    it "Int"    $ memSize @Int    >>= shouldBe 4
    it "Int8"   $ memSize @Int8   >>= shouldBe 1
    it "Int16"  $ memSize @Int16  >>= shouldBe 2
    it "Int32"  $ memSize @Int32  >>= shouldBe 4
    it "Int64"  $ memSize @Int64  >>= shouldBe 8
    it "Float"  $ memSize @Float  >>= shouldBe 4
    it "Double" $ memSize @Double >>= shouldBe 8

  describe "HasMemAlign" $ do
    it "()"     $ memAlign @()     >>= shouldBe 0
    it "Bool"   $ memAlign @Bool   >>= shouldBe 1
    it "Char"   $ memAlign @Char   >>= shouldBe 1
    it "Word8"  $ memAlign @Word8  >>= shouldBe 1
    it "Word16" $ memAlign @Word16 >>= shouldBe 2
    it "Word32" $ memAlign @Word32 >>= shouldBe 4
    it "Word64" $ memAlign @Word64 >>= shouldBe 8
    it "Int"    $ memAlign @Int    >>= shouldBe 4
    it "Int8"   $ memAlign @Int8   >>= shouldBe 1
    it "Int16"  $ memAlign @Int16  >>= shouldBe 2
    it "Int32"  $ memAlign @Int32  >>= shouldBe 4
    it "Int64"  $ memAlign @Int64  >>= shouldBe 8
    it "Float"  $ memAlign @Float  >>= shouldBe 4
    it "Double" $ memAlign @Double >>= shouldBe 8
