module Hobbes.Fregion.Ty.Desc.TestTypes ( spec_Ty_Desc_Types
                                        ) where


import Test.Tasty.Hspec

import Hobbes.Fregion.Ty.Desc
import Hobbes.Fregion.Ty.TH (makeDesc)

data VariantT = Chicken String
              | Dog Int
              | Fish Double
makeDesc ''VariantT

data VoidT
makeDesc ''VoidT

data UnitT = UnitT
makeDesc ''UnitT

data OneT = OneT String
makeDesc ''OneT

data DoubleT = DoubleT Int String
makeDesc ''DoubleT

data SumProdT = ProdC { name :: String
                      , ages :: Int
                      , friends :: [String]
                      }
              | Empty {}
              | Nothing
              | NestedVariant VariantT
              | OneInt Int
              | OneChar Char
              | OneString String
              | OneCharArr [Char]
              | DoubleIntChar Int Char

makeDesc ''SumProdT

newtype ProdSumProdT = ProdSumProdT { unProdSumProdT :: [[SumProdT]] }
makeDesc ''ProdSumProdT

spec_Ty_Desc_Types :: Spec
spec_Ty_Desc_Types = parallel $ do
  describe "HasDesc" $ do
    it "()" $ desc @() >>= shouldBe (MkPrimTy "unit" MkVoidTy)
    it "UnitT" $ desc @UnitT >>= shouldBe (MkSumTy [("UnitT",MkPrimTy "unit" MkVoidTy)])
    it "VoidT" $ desc @VoidT >>= shouldBe (MkVoidTy)
    it "OneT" $ desc @OneT >>= shouldBe (MkSumTy [("OneT",MkProdTy [(".f0",0,MkDArrTy (MkPrimTy "char" MkVoidTy))])])
    it "DoubleT" $ desc @DoubleT >>= shouldBe (MkSumTy [("DoubleT",MkProdTy [(".f0",0,MkPrimTy "int" MkVoidTy)
                                                                            ,(".f1",1,MkDArrTy (MkPrimTy "char" MkVoidTy))
                                                                            ])
                                                       ])
    it "SumProdT" $ desc @SumProdT >>= shouldBe (MkSumTy [("ProdC",MkProdTy [("name",0,MkDArrTy (MkPrimTy "char" MkVoidTy))
                                                                            ,("ages",1,MkPrimTy "int" MkVoidTy)
                                                                            ,("friends",2,MkDArrTy (MkDArrTy (MkPrimTy "char" MkVoidTy)))])
                                                         ,("Empty",MkPrimTy "unit" MkVoidTy)
                                                         ,("Nothing",MkPrimTy "unit" MkVoidTy)
                                                         ,("NestedVariant",MkSumTy [("Chicken",MkDArrTy (MkPrimTy "char" MkVoidTy))
                                                                                   ,("Dog",MkPrimTy "int" MkVoidTy)
                                                                                   ,("Fish",MkPrimTy "double" MkVoidTy)])
                                                         ,("OneInt",MkPrimTy "int" MkVoidTy)
                                                         ,("OneChar",MkPrimTy "char" MkVoidTy)
                                                         ,("OneString",MkDArrTy (MkPrimTy "char" MkVoidTy))
                                                         ,("OneCharArr",MkDArrTy (MkPrimTy "char" MkVoidTy))
                                                         ,("DoubleIntChar",MkProdTy [(".f0",0,MkPrimTy "int" MkVoidTy)
                                                                                    ,(".f1",1,MkPrimTy "char" MkVoidTy)])
                                                         ])
    it "ProdSumProdT" $ desc @ProdSumProdT >>= shouldBe (MkSumTy [("ProdSumProdT",MkProdTy [("unProdSumProdT",0,MkDArrTy (MkDArrTy (MkSumTy [("ProdC",MkProdTy [("name",0,MkDArrTy (MkPrimTy "char" MkVoidTy))
                                                                                                                                                               ,("ages",1,MkPrimTy "int" MkVoidTy)
                                                                                                                                                               ,("friends",2,MkDArrTy (MkDArrTy (MkPrimTy "char" MkVoidTy)))])
                                                                                                                                            ,("Empty",MkPrimTy "unit" MkVoidTy)
                                                                                                                                            ,("Nothing",MkPrimTy "unit" MkVoidTy)
                                                                                                                                            ,("NestedVariant",MkSumTy [("Chicken",MkDArrTy (MkPrimTy "char" MkVoidTy))
                                                                                                                                                                      ,("Dog",MkPrimTy "int" MkVoidTy)
                                                                                                                                                                      ,("Fish",MkPrimTy "double" MkVoidTy)])
                                                                                                                                            ,("OneInt",MkPrimTy "int" MkVoidTy)
                                                                                                                                            ,("OneChar",MkPrimTy "char" MkVoidTy)
                                                                                                                                            ,("OneString",MkDArrTy (MkPrimTy "char" MkVoidTy))
                                                                                                                                            ,("OneCharArr",MkDArrTy (MkPrimTy "char" MkVoidTy))
                                                                                                                                            ,("DoubleIntChar",MkProdTy [(".f0",0,MkPrimTy "int" MkVoidTy)
                                                                                                                                                                       ,(".f1",1,MkPrimTy "char" MkVoidTy)])])))])])
